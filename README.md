# Triwick-Servo Motor Controller API Documentation

## UART Communication

UART communication use 115200 baud with 8n1 configuration.

## Data Frame

The protocol use ASCII based communication and each frame must be terminated with \r\n character. 
> **[WARN]** After each command is issued, host must wait for response before next command can be issued.

### Command 
The frame configured as follows:

```
<keyword><space><param0><space><param1>...<paramN>\r\n
```
* Keyword is integer expressed in ASCII character, with maximum of 255. 
* Maximum 32 parameter can be sent in one frame, separated by space character. Each parameter is signed int32 expressed in ASCII character.

### Response
```
<keyword><space><apistatus><space><param0><space><param1>...<paramN>\r\n
```
* Keyword is the same keyword from command. Except if returned keyword is 255 which means unrecignized keyword
* API Status Code :
    * G : No Error
    * P : Number of parameter mismatch
    * W : Invalid parameter value
    * T : Undefined keyword
    * F : Wrong frame format
* Maximum 32 parameter can be sent in one frame, separated by space character. Each parameter is signed int32 expressed in ASCII character.

## Motor Mapping

For 2 DOF simulator purposes, motor mapped as follows:
* MOTOR_A : Left rig motor (0 to 180 degree range)
* MOTOR_B : Right rig motor (0 to 180 degree range)
* MOTOR_C : Steering wheel motor (-90 to 90 degree range)

## Axis convention

Axis Convention.
![Axis Convention Rig Motor A and B](https://gitlab.com/hardtmann/public/triwick-servo-api/-/raw/main/img/Axis%20convention.png)

Higher level command (API 15) use roll, pitch and steering as command.
Lower level command (API 1) use motor A, motor B and steering as command.

API 15 and API 1 will override each other.

## Units

* Position : 0.1*degree (e.g. 900 = 90 degree)
* Velocity : 0.1*degree/second
* Acceleration : 0.1*degree/second<sup>2</sup>
* Brightness : level (10 bit)

## API List

### Get Controller Status:
* Command: ```0\r\n```
* Return: ```0 [APIStatusCode] [ID] [Firmware Version]\r\n```
* Description:
Return board ID and firmware version. Board ID is unique to each PCB produced.

### Move motors to absolute position:
* Command: ```1 [TargetPosA] [TargetPosB] [TargetPosC]\r\n```
* Return : ```1 [APIStatusCode]\r\n```
* Description:  Move motor to absolute position, return W if one of these condition are met for any axis:
    * Target position > maximum position safety limit
    * Target position < minimum position safety limit

### Get Current Motor Position:
* Command: `2\r\n`
* Return: `2 [APIStatusCode] [PosA] [PosB] [PosC]\r\n`
* Description: Get actual current motor position.

### Set LED brightness
* Command: ```3 [RedLevel] [GreenLevel] [BlueLevel]\r\n```
* Return : ```3 [APIStatusCode]\r\n```
* Description: Set the brigthness level for each led color, return W if one of these condition are met for any axis:
    * Level is less than 0
    * Level is greater than 1023

### Get LED brightness
* Command: ```4\r\n```
* Return : ```4 [APIStatusCode] [RedLevel] [GreenLevel] [BlueLevel]\r\n```
* Description: Get the brigthness level for each led color.

### Get Motor Safety Limit
* Command: `5 [Axis]\r\n`
* Return: `5 [APIStatusCode] [MinPosLimit] [MaxPosLimit] [MaxVelocity] [MaxAcceleration]\r\n`
* Description: Get internal motor safety limit.

### Get Button Status
* Command: `12 [Axis]\r\n`
* Return: `12 [APIStatusCode] [ButtonState]\r\n`
* Description: Return button state, 1 if pressed, 0 if not pressed.

### Set rig position
* Command: ```15 [Pitch] [Roll] [SteeringAngle]\r\n```
* Return : ```15 [APIStatusCode]\r\n```
* Description:  Move rig to determined position
    * Max pitch : 5 degree (value 50), min pitch: -5 degree (value -50)
    * Max roll  : 10 degree (value 100), min roll: -10 degree (value -100)
    * Max steering angle: 90 degree (value 900), min steering angle: -90 degree (value -900)
    * Note : this api will not respect motor position safety limit and only respect constrain on above limit  

<br>

> **API marked with (DEV) used mainly for control system development and tuning. Use with caution.**

<br>

### (DEV) Set Control Loop params
* Command: `6 [Axis] [Kp] [Ki] [Kd] [SavetoEEPROM]\r\n`
* Return: `6 [APIStatusCode]\r\n`
* Description: Set control loop parameter for given axis. return W if one of these conditions are met:
    * Kp Ki or Kd less than 0
    * SavetoEEPROM is not 1 or 0
* Warn : 
Do not call this API with SavetoEEPROM==1 too often, the EEPROM lifetime is limited

### (DEV) Get Control Loop params
* Command: `7 [Axis]\r\n`
* Return: `7 [APIStatusCode] [Kp] [Ki] [Kd]\r\n`
* Description: Get control loop parameter for given axis.

### (DEV) Calibrate Encoder [WIP]
* Command: `8 [Axis] [MoveSteps] [delayUs]\r\n`
* Return: `8 [APIStatusCode] [isEncoderInverted] [MotorStepsPerRev] [isEncoderJumpDetected]\r\n`
* Description: Calibrate motor using encoder, also check encoder inversion against motor. Motor will read initial encoder position, then move with open loop control by [MoveSteps] & speed by [MoveSteps] & accel by [MoveSteps], then read final encoder position. This function will check the polarity of encoder compared to motor movement. If motor steps is positive, but the angular encoder value is decreased, then [isEncoderInverted] will be -1. [MotorStepsPerRev] is calculated by: total movement steps/total travel measured by encoder. Return W if encoder is not moving.

### (DEV) Set Servo Params
* Command: `9 [Axis] [isMotorInverted] [isEncoderInverted] [EncoderOffset] [MotorStepsPerRev] [SavetoEEPROM]\r\n`
* Return: `9 [APIStatusCode]\r\n`
* Description: Set encoder and motor control param.
* Warn : Do not call this API with SavetoEEPROM==1 too often, the EEPROM lifetime is limited

### (DEV) Get Servo Params
* Command: `10 [Axis]\r\n`
* Return: `10 [APIStatusCode] [isMotorInverted] [isEncoderInverted] [EncoderOffset] [MotorStepsPerRev]\r\n`
* Description: Get encoder and motor control param.
* Warn : Do not call this API with SavetoEEPROM==1 too often, the EEPROM lifetime is limited

### (DEV) Set Motor Safety Limit
* Command: `11 [Axis] [MinPosLimit] [MaxPosLimit] [MaxVelocity] [MaxAcceleration] [SavetoEEPROM]\r\n`
* Return: `11 [APIStatusCode]\r\n`
* Description: Set internal motor safety limit. MaxVel and MaxAccel is used for encoder corrected movement profile.
* Warn : Use with caution, possible damage to hardware if set incorrectly

### (DEV) Fallback to default config
* Command: `13\r\n`
* Return: `13 [APIStatusCode]\r\n`
* Description: Set all config back to default, rewriting eeprom.

### (DEV) Move Relative Open Loop
* Command: `14 [Axis] [DegreePerSecond] [DurationMs]\r\n`
* Return: `14 [APIStatusCode]\r\n`
* Description: Move axis using open loop stepper control.

### (DEV) Set Encoder Zero
* Command: `16 [Axis]\r\n`
* Return: `16 [APIStatusCode]\r\n`
* Description: Set current position of encoder as 0 position.
